﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;


namespace BRL
{
    public class MedicamentoBRL : AbstractBRL
    {
        Common.Medicamento med;

        public Medicamento Med { get => med; set => med = value; }
        public MedicamentoDAL dal { get => Dal; set => Dal = value; }

        MedicamentoDAL Dal;



      public  MedicamentoBRL()
        {
            Dal = new MedicamentoDAL();
        }
        public MedicamentoBRL( Medicamento med)
        {
            this.med = med;
            Dal = new MedicamentoDAL(med);
            
        }
        public override void Delete()
        {
            Dal.Delete();
        }

        public override void Insert()
        {
            dal.Insert();
        }

        public override DataTable Select()
        {
            return Dal.Select();
        }

        public override void Update()
        {
            Dal.Update();
        }
    
        public Medicamento Get(int idMedicamento)
        {
            return Dal.Get(idMedicamento);
        }

    }
}
