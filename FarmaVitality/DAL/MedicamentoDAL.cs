﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using MySql.Data.MySqlClient;

namespace DAL
{
    public class MedicamentoDAL : AbstractDAL
    {

        Medicamento med;
        //para insert
        public MedicamentoDAL (Medicamento med )
        {
            this.med = med;
        }
        public MedicamentoDAL()
        {

        }

        public Medicamento Med { get => med; set => med = value; }

        public override void Delete()
        {
            string query = @"update medicamento set estado=0 ,fechaActualizacion=CURRENT_TIMESTAMP,idUsuario=@idUsuario            
                             where idMedicamento=@idMedicamento";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idMedicamento", med.IdMedicamento);
                cmd.Parameters.AddWithValue("@idUsuario", med.IdUsuario);

                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override void Insert()
        {
            string query = @"insert into medicamento ( nombreMedicamento,fechaVencimiento,PrecioUnitario,cantidadMedicamentos,tipoMedicamento,idusuario)
                            values  (@nombreMedicamento,@fechaVencimiento,@PrecioUnitario,@cantidadMedicamentos,@tipoMedicamento,@idusuario)";

            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreMedicamento", med.NombreMedicamento);
                cmd.Parameters.AddWithValue("@fechaVencimiento", med.FechaVencimiento);
                cmd.Parameters.AddWithValue("@PrecioUnitario", med.PrecioUnitario);
                cmd.Parameters.AddWithValue("@cantidadMedicamentos", med.CantidadMedicamentos);
                cmd.Parameters.AddWithValue("@tipoMedicamento", med.TipoMedicamento);
                cmd.Parameters.AddWithValue("@idusuario", med.IdUsuario);


                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;

               
            }
        }

        public override DataTable Select()
        {
            DataTable res = new DataTable();


            //string query = @"  SELECT idMedicamento,nombreMedicamento AS 'NOMBRE DEL MEDICAMENTO' ,M.fechaVencimiento AS 'FECHA DE VENCIMIENTO',M.PrecioUnitario AS 'PRECIO UNITARIO',M.tipoMedicamento AS 'TIPO DE MEDICAMENTO' ,M.cantidadMedicamentos AS 'CANTIDAD DE MEDICAMENTOS',M.fechaRegistro AS 'LA FECHA DE REGISTRO',M.fechaActualizacion AS 'FECHA DE MODIFICACION',U.nombre AS 'MODIFICADO POR'
            //            FROM medicamento M
            //            INNER JOIN usuario U ON U.idUsuario
            //             where M.estado = 1";
            string query = @"      SELECT idMedicamento,nombreMedicamento AS 'NOMBRE MEDICAMENTO' ,M.fechaVencimiento AS 'FECH VENC',M.PrecioUnitario AS 'PRECIO UNIT',M.tipoMedicamento AS 'TIPO MEDICAMENTO' ,M.cantidadMedicamentos AS 'CANTIDAD MEDIC',M.fechaRegistro AS 'FECHA REGISTRO',M.fechaActualizacion AS 'FECHA MODIFICACION', CONCAT(U.nombre,' ',U.rol) AS 'MODIFICADO POR'
                        FROM medicamento M
                        INNER JOIN usuario U ON U.idUsuario
                         where M.estado = 1";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }

        public override void Update()
        {

            string query = @"update medicamento set   nombreMedicamento = @nombreMedicamento,PrecioUnitario = @PrecioUnitario,cantidadMedicamentos = @cantidadMedicamentos,tipoMedicamento = @tipoMedicamento,fechaActualizacion=current_timestamp,idusuario=@idusuario
                               where idMedicamento=@idMedicamento";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);

     
                cmd.Parameters.AddWithValue("@nombreMedicamento", med.NombreMedicamento);
                cmd.Parameters.AddWithValue("@PrecioUnitario", med.PrecioUnitario);
                cmd.Parameters.AddWithValue("@cantidadMedicamentos", med.CantidadMedicamentos);
                cmd.Parameters.AddWithValue("@tipoMedicamento", med.TipoMedicamento);
                cmd.Parameters.AddWithValue("@idMedicamento", med.IdMedicamento);
                cmd.Parameters.AddWithValue("@idusuario", med.IdUsuario);

                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public Medicamento Get(int idMedicamento)
        {
            Medicamento res = null;

            string query = @"SELECT idMedicamento,nombreMedicamento,fechaVencimiento,precioUnitario,cantidadMedicamentos,fechaRegistro,tipoMedicamento,estado,fechaActualizacion,idusuario
                                   from	 medicamento 
                                    where idMedicamento=@idMedicamento";
            MySqlCommand cmd = null;
            MySqlDataReader dr = null;

            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idMedicamento", idMedicamento);

                dr = Methods.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new Medicamento(int.Parse(dr[0].ToString()),dr[1].ToString(), DateTime.Parse(dr[2].ToString()), double.Parse(dr[3].ToString()), byte.Parse(dr[4].ToString()), DateTime.Parse(dr[5].ToString()), dr[6].ToString(),byte.Parse(dr[7].ToString()), DateTime.Parse(dr[8].ToString()),1);
                }

            }
            catch (Exception ex)
            {


                throw ex;
            }
            finally
            {
                dr.Close();
                cmd.Connection.Close();
            }

            return res;

        }
        public DataTable SelectIDName()
        {

            DataTable res = new DataTable();
            string query = @"select  idMedicamento,nombreMedicamento
                            from medicamento
                            where estado = 1
                            order by 2; ";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }


            return res;
        }
    }
}
