﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Common;
using DAL;



namespace DAL
{
   

    public class UsuariosDAL : AbstractDAL
    {
        private Usuario user;

        public Usuario User { get => user; set => user = value; }

        public UsuariosDAL(Usuario user)
        {
            this.User = user;
        }
        public UsuariosDAL()
        {

        }
       
        public override void Delete()
        {

            string query = @"update usuario set estado=0,fechaActualizacion=CURRENT_TIMESTAMP 
                                where idusuario=@idusuario";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idusuario", User.IdUsuario);

                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

      

        public override void Insert()

        {
        
            string query = @"INSERT INTO usuario (nombre,primerApellido,segundoApellido,telefono,direccion,email,rol,ci)
                                           VALUES (@nombre, @primerApellido, @segundoApellido,@telefono, @direccion,@email,@rol,@ci)";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);

                cmd.Parameters.AddWithValue("@nombre", User.Nombre);
                cmd.Parameters.AddWithValue("@primerApellido", User.PrimerApellido);
                cmd.Parameters.AddWithValue("@segundoApellido", User.SegundoApellido);              
                cmd.Parameters.AddWithValue("@telefono", User.Telefono);         
                cmd.Parameters.AddWithValue("@direccion", User.Direccion);
                cmd.Parameters.AddWithValue("@email", User.Email);               
                cmd.Parameters.AddWithValue("@rol", User.Rol);
                cmd.Parameters.AddWithValue("@ci", User.Ci);
               

                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public override DataTable Select()
        {

            DataTable res = new DataTable();
            //string query = @"SELECT idUsuario, CONCAT( nombre  ,' ',primerApellido , '',segundoApellido) AS 'NOMBRE COMPLETO' ,telefono AS 'NUMERO DE TELEFONO',direccion AS 'DIRECCION',email as 'CORREO ELECTRONICO',rol AS 'ROL',ci AS 'c i',fechaActualizacion AS 'FECHA DE ACTUALIZACION',fechaRegistro AS 'FECHA DE REGISTRO'
            //                 FROM usuario
            //                 WHERE Estado=1";

            string query = @"SELECT idUsuario,  CONCAT( nombre  ,' ',primerApellido , ' ',segundoApellido) AS 'NOMBRE COMPLETO' ,telefono AS 'NUMERO DE TELEFONO',direccion AS 'DIRECCION',email as 'CORREO ELECTRONICO',rol AS 'ROL',ci AS 'c i',fechaActualizacion AS 'FECHA DE ACTUALIZACION',fechaRegistro AS 'FECHA DE REGISTRO'
                       FROM usuario";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }
        public DataTable NombreUsuario()
        {

            DataTable res = new DataTable();

            //string query = @" SELECT idUsuario, CONCAT( primerApellido,nombre) AS ' USUARIO',concat(ci,123) AS 'CONTRASEÑA'
            //                  FROM usuario";

            string query = @"SELECT idUsuario, ci, CONCAT( primerApellido, nombre) AS ' USUARIO',concat(ci, 123) AS 'CONTRASEÑA'
                        FROM usuario";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }

        public override void Update()
        {
            string query = @"update usuario set nombre = @nombre, primerApellido = @primerApellido, segundoApellido = @segundoApellido, ci = @ci, telefono = @telefono, direccion = @direccion,email=@email,fechaRegistro=CURRENT_TIMESTAMP, rol = @rol,FechaActualizacion=CURRENT_TIMESTAMP
                            WHERE idUsuario=@idUsuario";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombre", User.Nombre);
                cmd.Parameters.AddWithValue("@primerApellido", User.PrimerApellido);
                cmd.Parameters.AddWithValue("@segundoApellido", User.SegundoApellido);
                cmd.Parameters.AddWithValue("@ci", User.Ci);
                cmd.Parameters.AddWithValue("@telefono", User.Telefono);
                cmd.Parameters.AddWithValue("@direccion", User.Direccion);
                cmd.Parameters.AddWithValue("@email", User.Email);
                cmd.Parameters.AddWithValue("@rol", User.Rol);
                cmd.Parameters.AddWithValue("@idUsuario", User.IdUsuario);

                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public Usuario Get(int IdUsuario)
        {
            
                Usuario res = null;

            string query = @"select idUsuario,nombre,primerApellido,segundoApellido,ci,telefono,direccion,email,fechaRegistro,rol,estado,fechaActualizacion
                                   from	 usuario 
                                    where idUsuario=@idUsuario";

            MySqlCommand cmd = null;
            MySqlDataReader dr = null;

            try
            {//
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idUsuario", IdUsuario);
                dr = Methods.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new Usuario(int.Parse(dr[0].ToString()),dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), int.Parse(dr[5].ToString()), dr[6].ToString(), dr[7].ToString(), DateTime.Parse(dr[8].ToString()), dr[9].ToString(), byte.Parse(dr[10].ToString()), DateTime.Parse(dr[11].ToString()));
                }

            }
            catch (Exception ex)
            {


                throw ex;
            }
            finally
            {
                dr.Close();
                cmd.Connection.Close();
            }

            return res;

        }
        public DataTable SELECTCI(string ci)
        {

            DataTable res = new DataTable();
            string query = @"SELECT idUsuario,ci, CONCAT( primerApellido,nombre) AS ' USUARIO',concat(ci,123) AS 'CONTRASEÑA'
                            FROM usuario
                            where ci LIKE @texto";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@texto","%"+ci+"%");
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            { throw ex; }
            return res;
        }
    
        #region
        public DataTable Login(string nombreUsuario, string password)
        {
            DataTable res = new DataTable();
            string query = @"SELECT idUsuario,nombreUsuario,rol,password from usuario
                                    where nombreUsuario=@nombreUsuario and password = md5(@password)";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreUsuario", nombreUsuario);
                cmd.Parameters.AddWithValue("@password", password);

                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {


                throw ex;
            }

            return res;



        }
        public DataTable NuevaPassword(string password)
        {
            DataTable res = new DataTable();
            string query = @"SELECT idUsuario,nombreUsuario,rol, password ,concat(nombre,'',primerApellido,'',ifnull(segundoApellido,'')'->',ci)

                             from usuario
                             where password=md5(@password)";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@password", password);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }


            return res;
        }
        public void Password(int id, string passw)
        {
            string query = @"UPDATE usuario SET Password=md5(@Password), FechaActualizacion=CURRENT_TIMESTAMP
                            WHERE idUsuario=@id;";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);

                cmd.Parameters.AddWithValue("@Password", passw);
                cmd.Parameters.AddWithValue("@id", id);
                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion

    }
}

