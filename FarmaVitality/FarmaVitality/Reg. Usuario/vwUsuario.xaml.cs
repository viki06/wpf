﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRL;
using DAL;
using System.Data;

namespace FarmaVitality.Usuario
{
    /// <summary>
    /// Lógica de interacción para vwUsuario.xaml
    /// </summary>
    public partial class vwUsuario : Window
    {

        byte opcion = 0;

        Common.Usuario user;
        UsuariosBRL brl;
        public vwUsuario()
        {
            InitializeComponent();
        }

        void Habilitar(byte opcion)
        {
            this.opcion = opcion;
            btnInsertar.IsEnabled = false;
            btnModificar.IsEnabled = false;
            btnElimiar.IsEnabled = false;

            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            dtDatos.IsEnabled = true;
            txtNombre.Focus();
        }
        void DesHabilitar()
        {
            btnInsertar.IsEnabled = true;
            btnModificar.IsEnabled = true;
            btnElimiar.IsEnabled = true;

            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            dtDatos.IsEnabled = false;
        }
        void LlenarDataGrid()
        {
            try
            {
                brl = new UsuariosBRL();
                dtDatos.ItemsSource = null;
                dtDatos.ItemsSource = brl.Select().DefaultView;
                dtDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "error al filtrar");
            }
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(1);
        }


        private void BtnGuardar_Click(object sender, RoutedEventArgs e)
        {
            switch (opcion)
            {
                case 1:
                    try
                    {
                        user = new Common.Usuario(txtNombre.Text, txtprimerAp.Text, txtSegundoApellido.Text, int.Parse(txtTelefono.Text), txtDireccion.Text, txtEmail.Text, txtRol.Text, txtCi.Text);
                        brl = new UsuariosBRL(user);
                        brl.Insert();
                        LlenarDataGrid();
                        MessageBox.Show("usuario insertado con exito...");

                        DesHabilitar();


                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("error al insert  " + ex.Message);
                    }
                    break;

                case 2:
                    try
                    {
                        user.Nombre = txtNombre.Text;
                        user.PrimerApellido = txtprimerAp.Text;
                        user.SegundoApellido = txtSegundoApellido.Text;
                        user.Ci = txtCi.Text;
                        user.Telefono = int.Parse(txtTelefono.Text);
                        user.Direccion = txtDireccion.Text;
                        user.Rol = txtRol.Text;
                        user.Email = txtEmail.Text;
                        brl = new UsuariosBRL(user);
                        brl.Update();



                        MessageBox.Show(" Modificado con exito...");
                        LlenarDataGrid();

                        DesHabilitar();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("Error al modificar " + ex.Message);
                    }
                    break;
            }
        }

        private void BtnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);
        }

        private void BtnElimiar_Click(object sender, RoutedEventArgs e)
        {

            if (dtDatos.SelectedItem != null && user != null)
            {
                if (MessageBox.Show("esta realmente segur@ de eleminar el registro??", "Eleminar", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        brl = new UsuariosBRL(user);
                        brl.Delete();                      
                        MessageBox.Show("Registro Eleminado con exito");
                        LlenarDataGrid();

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DesHabilitar();

        }



        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LlenarDataGrid();
        }

        private void DtDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            user = null;
            if (dtDatos.SelectedItems != null && dtDatos.Items.Count > 0)
            {
                try
                {
                    var dataRow = (DataRowView)dtDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new UsuariosBRL();
                    user = brl.Get(id);
                    if (user != null)
                    {
                        txtNombre.Text = user.Nombre;
                        txtprimerAp.Text = user.PrimerApellido;
                        txtSegundoApellido.Text = user.SegundoApellido;
                        txtCi.Text = user.Ci;
                        txtTelefono.Text = user.Telefono.ToString();
                        txtDireccion.Text = user.Direccion;
                        txtRol.Text = user.Rol;
                        txtEmail.Text = user.Email;

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al cargar datos a txt " + ex.Message);
                }

            }
        }

        private void BtnNombreUsuario_Click(object sender, RoutedEventArgs e)
        {
            Reg._Usuario.vwNombreUsuario vw = new Reg._Usuario.vwNombreUsuario();
            vw.ShowDialog();


        }



        Validaciones obj = new Validaciones();
        private void TxtNombre_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            obj.validarLetras(e);
        }

        private void TxtTelefono_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            obj.validarNumeros(e);
        }

        private void TxtprimerAp_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            obj.validarLetras(e);
        }

        private void TxtSegundoApellido_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            obj.validarLetras(e);
        }


        private void TxtRol_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            obj.validarLetras(e);
        }      
    }
}
 





