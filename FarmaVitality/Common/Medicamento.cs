﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Medicamento
    {


        #region atributos de la clase Medicamento
        private int idMedicamento;
        private string nombreMedicamento;
        private DateTime fechaVencimiento;
        private double precioUnitario;
        private byte cantidadMedicamentos;
        private DateTime fechaRegistro;
        private string tipoMedicamento;
        private byte estado;
        private DateTime fechaActualizacion;
        private int idUsuario;



        public int IdMedicamento { get => idMedicamento; set => idMedicamento = value; }
        public string NombreMedicamento { get => nombreMedicamento; set => nombreMedicamento = value; }
        public DateTime FechaVencimiento { get => fechaVencimiento; set => fechaVencimiento = value; }
        public double PrecioUnitario { get => precioUnitario; set => precioUnitario = value; }
        public DateTime FechaActualizacion { get => fechaActualizacion; set => fechaActualizacion = value; }
        public byte Estado { get => estado; set => estado = value; }
        public string TipoMedicamento { get => tipoMedicamento; set => tipoMedicamento = value; }
        public byte CantidadMedicamentos { get => cantidadMedicamentos; set => cantidadMedicamentos = value; }
        public DateTime FechaRegistro { get => fechaRegistro; set => fechaRegistro = value; }
        public int IdUsuario { get => idUsuario; set => idUsuario = value; }


        #endregion

        #region  sobre carga de constructor
        public Medicamento()
        {

        }

        /// <summary>
        /// constructor  completo
        /// </summary>
        /// <param name="idMedicamento"></param>
        /// <param name="nombreMedicamento"></param>
        /// <param name="fechaVencimiento"></param>
        /// <param name="precioUnitario"></param>
        /// <param name="cantidadMedicamentos"></param>
        /// <param name="fechaRegistro"></param>
        /// <param name="tipoMedicamento"></param>
        /// <param name="estado"></param>
        /// <param name="fechaActualizacion"></param>
        /// <param name="idUsuario"></param>

        public Medicamento(int idMedicamento, string nombreMedicamento, DateTime fechaVencimiento, double precioUnitario, byte cantidadMedicamentos, DateTime fechaRegistro, string tipoMedicamento, byte estado, DateTime fechaActualizacion, int idUsuario)

        {
            this.idMedicamento = idMedicamento;
            this.nombreMedicamento = nombreMedicamento;
            this.fechaVencimiento = fechaVencimiento;
            this.precioUnitario = precioUnitario;
            this.cantidadMedicamentos = cantidadMedicamentos;
            this.fechaRegistro = fechaRegistro;
            this.tipoMedicamento = tipoMedicamento;
            this.estado = estado;
            this.fechaActualizacion = fechaActualizacion;
            this.idUsuario = idUsuario;
        }
        /// <summary>
        /// constructor inser
        /// </summary>
        /// <param name="nombreMedicamento"></param>
        /// <param name="fechaVencimiento"></param>
        /// <param name="precioUnitario"></param>
        /// <param name="cantidadMedicamentos"></param>
        /// <param name="tipoMedicamento"></param>
        /// <param name="idUsuario"></param>
        public Medicamento( string nombreMedicamento, DateTime fechaVencimiento, double precioUnitario, byte cantidadMedicamentos,string tipoMedicamento, int idUsuario)
        {          
            this.nombreMedicamento = nombreMedicamento;
            this.fechaVencimiento = fechaVencimiento;
            this.precioUnitario = precioUnitario;
            this.cantidadMedicamentos = cantidadMedicamentos;
            this.tipoMedicamento = tipoMedicamento;           
            this.idUsuario = idUsuario;


        }
        public Medicamento(string nombreMedicamento, double precioUnitario, byte cantidadMedicamentos, string tipoMedicamento)
        {
            this.nombreMedicamento = nombreMedicamento;          
            this.precioUnitario = precioUnitario;
            this.cantidadMedicamentos = cantidadMedicamentos;
            this.tipoMedicamento = tipoMedicamento;     


        }
        #endregion
    }
}






